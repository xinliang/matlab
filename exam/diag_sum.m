function w=diag_sum(X)
[x,l]=size(X);
a=min(x,l);
g=(a+1)/2;
X=X([1:a],[1:a]);
L=flip(X);
if mod(a,2)==0
    s=sum(diag(X))+sum(diag(L));
else
    s=sum(diag(X))+sum(diag(L))-X(g,g);
end