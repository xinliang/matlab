function [Q]=intquad(n)
X=ones(n);
Q=[X*-1,X*exp(1);X*pi,X];
end
